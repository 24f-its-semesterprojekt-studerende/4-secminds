---
hide:
  - footer
---

## Information

Blue team implementerer sikkerhed og vedligeholder sikkerhed. De holder overblikket over virksomhedens aktiver og hvad der skal passes på. De leder kontinuerligt efter sikkerhedshuller og holder styr på at sikkerheden er effektiv samt opdateret.
På makro niveau er det hele organisationen som er medlem af blue team, brugere kan for eksempel være de første til at opdage et brud på sikkerheden.  
På mikro niveau er det de medarbejdere der er ansvarlige for at monitorere, forsvare og respondere på sikkerhedsbrud. 

Red team og blue team stammer så vidt jeg kan finde ud af fra [Lieutenant von Reisswitz’s Kriegsspiel](https://www.armchairdragoons.com/articles/research/nineteenth-century-military-war-games-lieutenant-von-reisswitzs-kriegsspiel/) hvor brikkerne i spillet var farvet hhv. røde (modstander) og blå (forsvar).

Blue team kan variere i størrelse. I mindre virksomheder kan det være få personer (måske kun en) som måske samtidigt har ansvar for traditionel it drift.  
Større virksomheder kan have deres egen sikkerhedsafdeling med et eller flere blue teams.  
Et blue team kan udover den daglige opretholdelse af det ønskede sikkerhedsniveau, indgå i et incident response team i tilfælde af et angreb.

Et blue team arbejder med mange forskellige dicipliner indenfor sikkerhed, for eksempel:

- Hærdning med f.eks CIS18 benchmarks
- Foranstaltninger på netværket (segmentering, monitorering, filtrering, firewalls, ids/ips)
- Administration og overvågning af logs
- SIEM systemer, evt. i en dedikeret SOC (Security Operations Center)
- Dokumentation i henhold til virksomhedens governance og compliance krav.

### Vidensdeling og kultur

Et blue team bør også være gode ambassadører i forhold til at etablere en sikkerhedskultur i en virksomhed. Det er ofte sådan at fagrupper i en virksomhed (tekniske og ikke tekniske) ikke er uddannet i sikkerhed og dermed ikke ser det som deres ansvar at bidrage til at opretholde virksomhedens sikkerhed. Selv hvis de opfatter det som deres ansvar har de måske ikke de nødvendige forudsætninger for at gøre det.  
Som eksempel kan du tænke tilbage på din egen tidligere uddannelse, var sikkerhed en del af den?  

Blue team har ofte fingeren på pulsen med hvad der sker indenfor it sikkerhed. Det kan være nyheder om angreb, de nyeste værktøjer til anomalidetektering i NIDS eller nye lovgivningstiltag der er på vej.  
Derfor bør blue team også forsøge at kommunikere deres viden om sikkerhed til virksomhedens medarbejdere på en måde som motiverer og engagerer således at alle bidrager til at opretholde sikkerheden.  

### MITRE Attack

[Mitre Att&ck](https://attack.mitre.org/) bruges af blue team til at studere hvordan forskellige trusselsaktører arbejder samt til at få viden om TTP (taktikker, teknikker og procedurer).  
For hver teknik er der forslag til hvordan teknikken kan mitigeres (afbødes) og detekteres. 

Se for eksempel under teknikken [T1040 Network Sniffing](https://attack.mitre.org/techniques/T1040/) hvordan det kan mitigeres ved:

- [M1041 Kryptering af følsom information](https://attack.mitre.org/mitigations/M1041)
- [M1032 Multi Factor Autentificering - MFA](https://attack.mitre.org/mitigations/M1032)
- [M1030 Netværks segmentering](https://attack.mitre.org/mitigations/M1030)
- [M1018 User Account Management - UAC](https://attack.mitre.org/mitigations/M1018)

Forslagene til at detektere [T1040 Network Sniffing](https://attack.mitre.org/techniques/T1040/) er:

- [DS0017 Monitorér eksekvering af kommandoer der bruges til netværkssniffing](https://attack.mitre.org/datasources/DS0017)
- [DS0009 Monitorér oprettelse af processer der der kan anvendes til netværkssniffing](https://attack.mitre.org/datasources/DS0009)

### Cyber kill chains og Pyramid of pain

Blue team kan anvende kill chains til at analysere de forskellig faser i et angreb. Jo tidligere angrebet kan afbrydes/forhindres af blue team jo mindre skade kan red team/trusslesaktøren gøre.  
Kill chain faser og specifikke taktikker og teknikker kan bruges til at identificere angreb. Et fagbegreb er [IOC - Indicators of compromise](https://en.wikipedia.org/wiki/Indicator_of_compromise) som er en artifakt på et netværk eller i et computersystem der indikerer en kompromittering.  

Trusselsaktører bruger tid og ressourcer på at udvikle deres TTP (teknikker, taktikker og procedurer). Tænk for ekempel på en trusselsaktør der bruger måneder på at opdage 0-day sårbarheder i et specifikt firewall produkt og efterfølgende udvikle exploits.  
Herefter skal de bruge ressourcer på at identificere virksomheder der anvender firewall produktet for at kunne udnytte det.  
Jo bedre blue team er til at genkende en specifik trusselsaktør jo mere præcist kan de respondere på den TTP som den trusselsaktør er kendt for at anvende.  

Til at illustrere det kan [David J Bianco's](https://www.blogger.com/profile/09760835714791462863) "Pyramid of pain" anvendes. 

![Pyramid-of-Pain-v2](../../images/networksec/uge18/111/Pyramid-of-Pain-v2.png)
_Kilde: [Enterprise Detection & Response](http://detect-respond.blogspot.com/2013/03/the-pyramid-of-pain.html)_

Måden pyramiden skal læses på er hvor svært det er for en trusselsaktør at omgås en detektering og fortsætte angrebet.  

Set fra bunden af pyramiden er det for eksempel ret let at ændre et stykke malware, så dets hash værdi er anderledes end den hashværdi, som du har konfigureret dit signaturbaserede NIDS til at detektere. Det vil sige at trusselsaktøren forholdvist ubesværet kan omgås dit forsvar.  

Et andet eksempel er midt i pyramiden, "Domain Names". Det er stadig simpelt for trusselsaktøren at omgås din firewalls deny liste ved at skifte til et domænenavn som du ikke blokerer, det tager længere tid end at ændre en hash værdi men det kan lade sig gøre.   

Hvis du derimod er i stand til at detektere og respondere på TTP niveau vil du være i stand til at forudsige trusselsaktørens adfærd og dermed sætte alle deres udviklede ressourcer ud af spil.  
Trusselsaktøren vil dermed have to valg, at give op eller at genopfinde dem selv forfra med alt det indebærer af teknikker, taktikker og procedurer.    

## Instruktioner

Nu ved i lidt om blue team, i ved faktisk meget om deres arbejde fordi i gennem semestret har implementeret forskellige sikkerheds foranstaltninger på jeres proxmox server.  

1. Lav en brainstorm i jeres gruppe over hvordan i kan bruge blue teaming i jeres semester projekt? For eksempel:
    - Beskriv hvilke sikkerhedstiltag i har implementeret i netværket (hvordan dokumenteres det med henblik på løbende vedligehold?)
    - Kan i implementere foranstaltninger der er målrettet en specifik trusselsaktør eller Mitre Attack TTP ?
    - Hvordan kan i anvende Mitre Attack på blue team?
2. Skriv resultatet af drøftelserne i jeres dokumentation
3. Forbered jer på at præsentere resultaterne på klassen (vi tager en kort runde).

## BrainStorm

- Implementer Mitigations
- Red + blue team
- Brug Wazuh til detektering evt. af Brute Force
- Awareness kampagne
- Red team teknikker -> bryd ind -> blue team mitigations -> prøv igen -> shutdown?
- CIS18 kontroller
- NIS? ISO 27000?

## Ressourcer

- [Wikipedia: Blue team](https://en.wikipedia.org/wiki/Blue_team_(computer_security))
- [Enterprise Detection & Response (Pyramid of pain)](http://detect-respond.blogspot.com/2013/03/the-pyramid-of-pain.html)
- [THM: Intro to Defensive Security](https://tryhackme.com/r/room/defensivesecurity)
- [VIDEO: What is a Blue team?](https://youtu.be/kHLbmmPBBCg?si=PR2QJGEaJ1gGFYqQ)