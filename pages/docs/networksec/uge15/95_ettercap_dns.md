---
hide:
  - footer
---

## Information

Ettercap er et tool til at lave forskellige typer man-in-the-middle angreb. Den kan bruges til "arp spoofing" men også til at lave DNS Man In The Middle.  

DNS Man In The Middle gør det muligt for en trusselsaktør at "redirecte" til en falsk hjemmeside/ip adresse ved at omdirrigere DNS opslag til en valgfri IP adresse.  

Du kan læse mere om teknikken i ressourcer nederst i øvelsen.

Hvis du sidder fast i øvelsen kan denne artikel være en hjælp: 
[DNS POISONING USING ETTERCAP](https://ifediniruozioma.medium.com/dns-poisoning-using-ettercap-fb8d284784cb)

## Instruktioner

1. Start to virtuelle maskiner på samme netværk, den ene skal være en Kali maskine

    Den første Kali kalder vi "kali" og den anden "victim"

3. Find MAC og ip adresserne på både kali og victim

    Notér dem ned

4. Tilføj følgende linier i `/etc/ettercap/etter.dns` på kali maskinen, erstat `192.168.186.128` med ip på din kali maskine.

    ```
    www.google.com   PTR     192.168.186.128
    *.google.com    A       192.168.186.128 
    google.com      A       192.168.186.128
    dnsspoof.test   A       8.8.8.8 3600
    ```

5. Start ettercap

    Stop sniffing

    Scan for host ip adresser

    Tilføj Victim som target 1 og router/default gateway som target 2.

    Enable "dns_spoof" plugin

    Start ARP poisoning

    Start sniffing

6. Test at spoofing virker: `ping dnsspoof.test`

7. Start `ping google.com` på victim

    Kan det ses i wireshark på kali? Er der noget at bemærke i output fra ettercap eller i terminalen?

8. Giv dine bedste bud på hvordan DNS spoofing kan mitigeres. Ressourcerne herunder kan hjælpe

**NB.** Ettercap skal genstartes når man opdaterer `etter.dns`.

**BONUS** Hvis du vil lege mere med spoofing er der et tryhackme rum [L2 MAC Flooding & ARP Spoofing](https://tryhackme.com/r/room/layer2)

## Ressourcer

- [DNS Spoofing (Imperva)](https://www.imperva.com/learn/application-security/dns-spoofing/)
- [Mitre ATTACK DNS](https://attack.mitre.org/techniques/T1071/004/)
- [Wikipedia DNSSEC](https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions)
- [ICANN DNSSEC](https://www.icann.org/resources/pages/dnssec-what-is-it-why-important-2019-03-05-en)
- [Cloudflare dns encryption explained/](https://blog.cloudflare.com/dns-encryption-explained/)

