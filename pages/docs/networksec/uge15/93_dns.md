---
hide:
  - footer
---

## Information

Domain Name System - DNS bruger du hver dag, det samme gør sikkerhedsprofesionelle og trusselsaktører.  
Formålet med denne Øvelse er at få indsigt i hvordan DNS grundlæggende fungerer.

## Instruktioner

1) Gennemfør rummet [https://tryhackme.com/room/dnsindetail](https://tryhackme.com/room/dnsindetail)

2) Skriv dine opdagelser ned undervejs og dokumenter det på gitlab

Besvar herefter følgende om DNS og dokumenter undervejs:

1) Forklar med dine egne ord hvad DNS er og hvad det bruges til.

- Domain Name System (DNS) bruges til at oversætte domain names til IP-addresser.

2) Hvilken port benytter DNS pr. default?

- 53

3) Beskriv DNS domæne hierakiet

- Root Domain
- TLD (Top-Level-Domain) (shop.tryhackme**dotcom**)
- Second Level Domain (shop.**tryhackme**.com)
- Subdomain (**shop**.tryhackme.com)

4) Forklar disse DNS records: A, AAAA, MX, TXT og CNAME.

- A: Til IPv4 mapping

- AAAA: IPv6 mapping

- CNAME: Mapper til et andet domain name

- MX: Mapper til emailaddresser til emailservere 

- TXT: Liste af servere som har autoritet til at sende emails for domainet

5) Brug `nslookup` til at undersøge hvor mange mailservere ucl.dk har

![nslookup_ucl](../../images/networksec/uge15/93/ucl_mailserver.png)

6) Brug `dig` til at finde txt records for ucl.dk 

![dig_ucl](../../images/networksec/uge15/93/dig.png)

7) Brug wireshark til:

7.1) Filtrere DNS trafik. Dokumenter hvilket filter du skal bruge for kun at se DNS trafik

- DNS

7.2) Lav screenshots af eksempler på DNS trafik 
 
![dns_wireshark](../../images/networksec/uge15/93/dns.png)

## Ressourcer

- [Video - How a DNS Server (Domain Name System) works](https://youtu.be/mpQZVYPuDGU?feature=shared)
- [RFC1034](https://tools.ietf.org/html/rfc1034)
- [RFC1035](https://tools.ietf.org/html/rfc1035)
- [Cloudflare - DNS Records](https://www.cloudflare.com/learning/dns/dns-records/)
- [Video - DNS records](https://youtu.be/6uEwzkfViSM?feature=shared)
- [12 DNS Records Explained](https://www.techopedia.com/2/28806/internet/12-dns-records-explained)
- [dig](https://linux.die.net/man/1/dig)