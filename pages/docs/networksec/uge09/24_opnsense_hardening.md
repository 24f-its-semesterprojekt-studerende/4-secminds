---
hide:
  #- footer
---

## Information
CIS (Center for Internet Security) Critical Security Controls, også kendt som CIS Controls, er en omfattende liste over anbefalinger og bedste praksis til at forbedre sikkerheden i informationssystemer.  
CIS Controls er designet til at hjælpe organisationer med at beskytte sig mod de mest almindelige trusler og angreb.  
CIS18 er nyeste udgave af kontrollerne og kan findes hos [cisecurity.org](https://www.cisecurity.org/controls/cis-controls-list)

Når vi taler om CIS18-benchmarks, refererer det til de specifikke krav og retningslinjer, der er opstillet inden for CIS Control 18.  

## Instruktioner

1. Beslut i gruppen hvilken opnsense instans i vil bruge til at udføre hærdning. Måske vælger i at lave en klon af en eksisterende, eller i vælger måske at lave et *snapshot* af en eksisterende inden i ændrer konfigurationen.  
Under alle omstændigheder er det en god ide at overveje om den instans i arbejder på er en i får brug for senere, det kan jo være at i kommer til at ødelægge den ved en fejl?

- Ved opdatering, gem nuværende instans.

2. Find filen **CIS_pfSense_Firewall_Benchmark_v1.1.0.pdf** på itslearning og skim den individuelt
3. I gruppen snak om hvilke firewall benchmarks (fra dokumentet i punkt 2) i vil implementere på routeren. Vælg så mange som muligt.

- 4.1 Firewall Rules Policies

<hr style="height:2px;border-width:0;color:gray;background-color:gray">

## Der tages udgangspunkt i 4.1 Firewall Rules Policy

### 4.1.1 Ensure no Allow Rule with Any in Destination Field present in the Firewall Rules (Manual)
#### Profile Applicability
- Level 1

#### Description
The Firewall Rules with Any in Source field allows all the IP Addresses of the Network to access the specified destination configured in the Firewall rules for specific services.

#### Rationale
In any properly designed and implemented Milner Firewall architecture the connection that loads the service should be explicitly allowed from the IP source address to the specificIP destination address. This services abolishes the chances of an exploit because of service misconfigurations.

#### Audit
Verify there are no allowed rules present in the firewall which has Any used in the Destination field.

In CLI:

<code>pfctl -sr<br></code>

![pfctl-sr-before](../../images/networksec/uge09/23_opnsense_hardening/pfctl.png)

![GUI-before](../../images/networksec/uge09/23_opnsense_hardening/4-1-1before.png)

#### Remediation
Delete or Disable the rule from the firewall which has Any used in the Destination field.

![pfctl-sr-after](../../images/networksec/uge09/23_opnsense_hardening/pfctlafter.png)

![GUI-after](../../images/networksec/uge09/23_opnsense_hardening/4-1-1after.png)

#### Default Value
Default "Allow Any" rule to prevent lockout.Default "Allow Any" rules on LAN Interfaces to allow network connectivity within the LAN.

#### CIS Controls

| Control | IG 1 | IG 2 | IG 3 |
| :------| :---:|:----:|:----:|
| 4.2 Establish and Maintain a Secure Configuration Process for Network Infrastructure | x | x | x |
| 4.4 Implement and Manage a Firewall on Servers | x | x | x |
| 4.5 Implement and Manage a Firewall on End-User Devices | x | x | x |

<hr style="height:2px;border-width:0;color:gray;background-color:gray">

### 4.1.2 Ensure no Allow Rule with Any in Source field present in the Firewall Rules (Manual)

#### Profile Applicability
- Level 1

#### Description
The Firewall Rules with Any in the Destination field allows accessing all the IP Addresses of Network from specified Sources configured in the Firewall rules for specific services

#### Rationale
Ideally, the traffic should be explicitly allowed from the specific Source to specific Destination for the required services. This provides better control over the traffic passes through the firewall and reduce the chances of an exploit because of service misconfiguration.

#### Audit
Verify there are no allowed rules present in the firewall which has Any used in theSource field. If there is any such rule present in the firewall, it shouldhave a business justification and also it should be documented

In CLI:

`` pfctl -sr ``

![pfctl-sr-before](../../images/networksec/uge09/23_opnsense_hardening/pfctl.png)

![GUI-before](../../images/networksec/uge09/23_opnsense_hardening/4-1-1before.png)

Ingen ændringer krævet.

#### Remediation
Delete the rule from the firewall which has Any used in the Source field.Default Value:Default "Allow Any" rule to prevent lockout.Default "Allow Any" ruleson LAN Interfaces to allow network connectivity within the LAN.

#### CIS Controls:
| Control | IG 1 | IG 2 | IG 3 |
| :------| :---:|:----:|:----:|
| 4.2 Establish and Maintain a Secure Configuration Process for Network Infrastructure | x | x | x |
| 4.4 Implement and Manage a Firewall on Servers | x | x | x |
| 4.5 Implement and Manage a Firewall on End-User Devices | x | x | x |

<hr style="height:2px;border-width:0;color:gray;background-color:gray">

### 4.1.3 Ensure no Allow Rule with Any in Services field present in the Firewall Rules (Manual)
#### Profile Applicability
- Level 1

#### Description
The Firewall Rules with Any in the Service field allows accessing all the Services from specified Source to specified Destination configured in the Firewall rules.

#### Rationale
Many services, including telnet, FTP, and TFTP, have security problems. Attackers can utilize these services to their advantage in order to access computers, obtain credentials, or launch DoS attacks. These services must be set up in accordance with the requirements of the company.

#### Audit
Verify there are no allowed rules present in the firewall which has Any used in the Servicefield.

In CLI:

`` pfctl -sr `` 

![pfctl-sr-before](../../images/networksec/uge09/23_opnsense_hardening/pfctl.png)

![GUI-before](../../images/networksec/uge09/23_opnsense_hardening/4-1-1before.png)

#### Remediation
Delete the rule from the firewall which has Any used in the Service field.Default Value:Default "Allow Any" rule to prevent lockout.Default "Allow Any" rules on LAN Interfaces to allow network connectivity within the LAN.

![TCP](../../images/networksec/uge09/23_opnsense_hardening/4-1-3.png)

![GUI-after](../../images/networksec/uge09/23_opnsense_hardening/4-1-3portchange.png)

#### CIS Controls:
| Control | IG 1 | IG 2 | IG 3 |
| :------| :---:|:----:|:----:|
| 4.2 Establish and Maintain a Secure Configuration Process for Network Infrastructure | x | x | x |
| 4.4 Implement and Manage a Firewall on Servers | x | x | x |
| 4.5 Implement and Manage a Firewall on End-User Devices | x | x | x |

<hr style="height:2px;border-width:0;color:gray;background-color:gray">

### 4.1.4 Ensure there are no Unused Policies (Manual)
#### Profile Applicability
- Level 1

#### Description
Ensure that there are no firewall policies that are unused

#### Rationale
Unused policies may provide unintended or anticipated access to services or hosts

#### Audit
Review all Firewall policies for use and validate the purpose of the policy.


![GUI-before](../../images/networksec/uge09/23_opnsense_hardening/4-1-3.png)

Ingen ubrugte policies.

#### Remediation
Disable and then delete any unused firewall policies

#### CIS Controls
| Control | IG 1 | IG 2 | IG 3 |
| :------| :---:|:----:|:----:|
| 4.8 Uninstall or Disable Unnecessary Services on Enterprise Assets and Software |  | x | x |

<hr style="height:2px;border-width:0;color:gray;background-color:gray">

### 4.1.5 Ensure Logging is Enable for All Firewall Rules (Manual)
#### Profile Applicability
- Level 1

#### Description
Ensure all firewall rules have logging enable.

#### Rationale
The event log of firewall rules helps in identifying the allowed and blocked traffic and also helps in troubleshooting and forensic investigation. It is always good to enablelogging for all the firewall rules, but by logging multiple firewall rules results in a huge log files, which requires huge disk space and management operations. Logs play an important role in security auditing, incident response, system maintenance and forensic investigation, and should be configured as per the business needs.

#### Audit
In the GUI:

`` - Navigate to Status > System Logs > Settings ``<br>
`` - Verify Desired Logging Options are enabled `` 

#### Remediation
In the GUI:

`` - Navigate to Status > System Logs > Settings ``<br>
`` - Verify Desired Logging Options to enable `` 

System: Settings: Logging (OPNsense)

![Logging](../../images/networksec/uge09/23_opnsense_hardening/4-1-5.png)

#### Default Value
The following log options are enabled by default: Log firewall default blocks:

- Log packets matched from the default block rules in the ruleset
- Log packets blocked by 'Block Bogon Networks' rules
- Log packets blocked by 'Block Private Networks' rules Web Server Log:
- Log errors from the web server process Log Configuration Changes:
- Generate log entries when making changes to the configuration.

#### CIS Controls
| Control | IG 1 | IG 2 | IG 3 |
| :------| :---:|:----:|:----:|
| 3.8 Document Data Flows |  | x | x |
| 8.2 Collect Audit Logs | x | x | x |

<hr style="height:2px;border-width:0;color:gray;background-color:gray">

### 4.1.6 Ensure ICMP Request is securely configured (Manual)
#### Profile Applicability:
- Level 1

#### Description
ICMP (Internet Control Message Protocol) is used by network devices to communicate with each other. While ICMP is essential for network management and troubleshooting, allowing all types of ICMP requests may increase the risk of attacks such as Ping Flood andICMP Redirect. Therefore, it is recommended to securely configure the allowed types of ICMP requests on the firewall.

#### Rationale
Allowing all types of ICMP requests may increase the risk of attacks such as Ping Flood and ICMP Redirect. To mitigate these risks, it is recommended to securely configure the allowed types of ICMP requests on the firewall.

#### Audit
In GUI:

``- Navigate to Firewall > Rules``<br>
``- Review the firewall rules to determine if any ICMP request rules are present``<br>
``- Check the configured ICMP request types for each rule to ensure that only necessary types are allowed``

#### Remediation
In GUI:

``- Navigate to Firewall > Rules``<br>
``- Review the firewall rules to determine if any ICMP request rules are present``<br>
``- Check the configured ICMP request types for each rule to ensure that only necessary types are allowed``

Default protokol ved oprettelse er "Any", så vi ændrer den til "ICMP".

![ICMP](../../images/networksec/uge09/23_opnsense_hardening/4-1-6-icmp.png)

![GUI-final](../../images/networksec/uge09/23_opnsense_hardening/GUI-final.png)

#### CIS Controls
| Control | IG 1 | IG 2 | IG 3 |
| :------| :---:|:----:|:----:|
| 12.6 Use of Secure Network Management and Communication Protocols |  | x | x |

<hr style="height:2px;border-width:0;color:gray;background-color:gray">

## Links
- [CIS18 benchmarks](https://www.cisecurity.org/cis-benchmarks)
- [CIS Controls Assessment Specification](https://controls-assessment-specification.readthedocs.io/en/stable/index.html)