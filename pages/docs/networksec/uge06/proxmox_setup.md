---
 hide:
#   - footer
---

# Hardware opgrading og Proxmox Setup

## Information

- Dokumentation for opgradering af hardware og opsætning af Proxmox server

## Instruktioner

### Ram opgradering
- Rammen kan tilgås efter beskyttelseslaget fjernes.
- Rammen klikkes ud og tages op.
- Ny ram installeres ved at trykke den ned, indtil klik høres.

![Ram Beskyttelse](../../images/Hardware-Proxmox/IMG20240202092251.jpg)

### Harddisk opgradering
- Harddisken kan tilgås ved at holde de to blå knapper nede, således at forsidepanelet kan tages af.
- Dernæst holdes låsen nede for den individuelle harddisk-case.
- Harddisken kan nu fjernes fra dens case, ved at vippe den ene side.
- Ny harddisk installeres i dens case ved at vippe den ene side, og presse den ned (Blå knapper skal måske omrokeres)
- Harddiskcasen installeres i tårnet samme måde som det blev fjernet.

### Proxmox opsætning
- USB indsættes
- PC genstartes således 'Proxmox installer' starter, vælg grafisk installation
- Følgende udfyldes:
    - Email: keha29384@edu.ucl.dk
    - Password: Vores password...
    - Hostname: secmindsproxmox.netlab.ucl.dk 
    - Gateway: 10.56.16.1/22
    - DNS: 8.8.8.8
- PC genstartes
- Der logges ind med
    - Login: root
    - Password: Vores password...
## Links
- [Billeder](https://drive.google.com/drive/u/1/folders/1HuhAEbVXbW4Uqpg2OFb9tFMeyIAjaQBS)
- [Proxmox Setup Guide](https://www.proxmox.com/en/proxmox-virtual-environment/get-started)