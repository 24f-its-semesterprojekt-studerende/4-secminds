---
 hide:
#   - footer
---

1) Hvad betyder LAN?

- Local Area Network

2) Hvad betyder WAN?

- Wide Area Network

3) Hvor mange bits er en ipv4 adresse?

- 8 * 4 = 32

4) Hvor mange forskellige ipv4 adresser findes der?

- 2^32

5) Hvor mange bits er en ipv6 adresse?

- 8 * 16 = 128

6) Hvad er en subnet maske?

- F.eks. /24, tallet bestemmer hvor mange IP-addresser hører til netværksdelen; de resterende hører til host.

7) Hvor mange hosts kan der være på netværket 10.10.10.0/24

- 2^(32-24) - 2 = 254 (0: Network, 255: Broadcast)

8) Hvor mange hosts kan der være på netværket 10.10.10.0/22

- 2^(32-22) - 2 = 1022 (0: Network, 255: Broadcast)

9) Hvor mange hosts kan der være på netværket 10.10.10.0/30

- 2^(32-30) - 2 = 2 (0: Network, 255: Broadcast)

10) Hvad er en MAC adresse?

- Media Access Control Address (12 digit hexadecimal for hvert netværkskort)

11) Hvor mange bits er en MAC adresse?

- 8 * 6 = 48

12) Hvilken MAC adresse har din computers NIC?

- ipconfig /all -> Ethernet adapter -> Physical address

13) Hvor mange lag har OSI modellen ?

    7) Application Layer

    6) Presentation Layer

    5) Session Layer

    4) Transport Layer

    3) Network Layer

    2) Data Link Layer

    1) Physical Layer

14) Hvilket lag i OSI modellen hører en netværkshub til?

- 1, Physical layer

15) Hvilket lag i OSI modellen hører en switch til?

- 2, Data Link Layer

16) Hvilket lag i OSI modellen hører en router til?

- 3, Network Layer

17) Hvilken addressering anvender en switch?

- MAC-addresser

18) Hvilken addressering anvender en router?

- IP-addreser

19) På hvilket lag i OSI modellen hører protokollerne TCP og UDP til?

- 4, Transport Layer

20) Hvad udveksles i starten af en TCP forbindelse?

- 3 way handshake, SYN, SYN/ACK, ACK

21) Hvilken port er standard for SSH?

- 22

22) Hvilken port er standard for https?

- 443

23) Hvilken protokol hører port 53 til?

- TCP/UDP

24) Hvilken port kommunikerer OpenVPN på?

- 1194

25) Er FTP krypteret?

- Nej

26) Hvad gør en DHCP server/service?

- Dynamic Host Configuration Protocol; Giver IP-addresse og gateway

27) Hvad gør DNS?

- Oversætter IP-addresser til menneskelig tekst

28) Hvad gør NAT?

- Network Address Translation; Lokale IP-addresser kortlægges til offentlige IP-addresser

29) Hvad er en VPN?

- Virtual Private Network; geolokationsændring

30) Hvilke frekvenser er standard i WIFI?

- 2.4 GHz, 5 GHz, 6 GHz

31) Hvad gør en netværksfirewall ?

- Den laver ild, så man ikke kan gå igennem uden at dø

32) Hvad er OPNsense?

- Firewall og routing software