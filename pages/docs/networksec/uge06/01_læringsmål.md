---
 hide:
#   - footer
---

### Viden 
- Netværkstrusler
- Trådløs sikkerhed 
- Sikkerhed i TCP/IP 
- Adressering i de forskellige lag 
- Dybdegående kendskab til flere af de mest anvendte internet protokoller (ssl) 
- Hvilke enheder, der anvender hvilke protokoller 
- Forskellige sniffing strategier og teknikker 
- Netværk management (overvågning/logning, snmp) 
- Forskellige VPN setups 
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI).

### Færdigheder 
- Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot) 
- Teste netværk for angreb rettet mod de mest anvendte protokoller 
- Identificere sårbarheder som et netværk kan have.    

### Kompetencer  
- Designe, konstruere og implementere samt teste et sikkert netværk 
- Monitorere og administrere et netværks komponenter 
- Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report) 
- Opsætte og konfigurere et IDS eller IPS.  
- Det giver gode forudsætninger for at komme ind og lære mere på arbejdsmarkedet.

## Links
[Studiedokumenter](https://www.ucl.dk/studiedokumenter/it-sikkerhed)