---
 hide:
#  - footer
---

## Information
Øvelsen er individuel.

I denne øvelse skal du arbejde praktisk med segmentering af netværk i opnsense.
Formålet er at lære hvordan du kan udvide opnsense med et ekstra netværksinterface og tilhørende netværk.

## Instruktioner
1) Sluk for opnsense VM. Tilføj et ekstra netværksinterface på samme måde som da du konfigurerede maskinen i Øvelse 20 - OPNsense på vmware workstation

2) Tilslut netværksinterfacet til vmnet3.

3) Tænd for opnsense og konfigurer det nye interface som nedenstående:

- Assign nyt interface, kald det management
- Assign nyt interface
- Konfigurer interfacet som følgende
- Tænd for DHCP på netværket og tildel følgende DHCP range

4) Tilslut en ny VM til vmnet3 og bekræft at maskinen får tildelt en IP i den ip range du har konfigureret på interfacet.  

## Links
- [How to Create a Basic DMZ (Demilitarized Zone) Network in OPNsense](https://homenetworkguy.com/how-to/create-basic-dmz-network-opnsense/)
- [OPNsense docs - Interfaces](https://docs.opnsense.org/interfaces.html)