---
 hide:
#   - footer
---

## Instruktioner
1) I kali åbn wireshark og lyt på eth0 interfacet indtil i har sniffet en god mængde trafik. Hvis der ikke kommer meget trafik kan i åbne browseren på kali og browse et par sider (gerne nogen der bruger http og ftp hvis i kan finde det) så burde der komme en del trafik.


2) Tryk på stop knappen i wireshark (den røde firkant)


3) Gem trafikken som en .pcapng fil på kali maskinen i documents mappen


4) Lav en liste over de protokoller der optræder i trafikken og placer protokollerne i OSI modellen. (her kan statistics menuen hjælpe med at danne et overblik over protokoller)
wireshark protocols

- ICMP6 (3)
- TCP (4)
- TLSv1.2 (4)
- TLSv1.3 (4)
- UDP (4)
- QUIC (Quick UDP Internet Connection) (4)
- OCSP (Online Certificate Status Protocol) (4)

5) Find minimum et eksempel, med kildehenvisning, på hvordan en af jeres fundne protokoller kan misbruges af en trusselsaktør, husk at være kritiske med den kilde i anvender til at underbygge jeres påstand.

- [UDP_CISA](https://www.cisa.gov/news-events/alerts/2014/01/17/udp-based-amplification-attacks)