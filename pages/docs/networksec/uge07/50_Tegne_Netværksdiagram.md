---
 hide:
#   - footer
---

### Logisk diagram

![50_Logisk_diagram](../../images/networksec/uge07/50_netværksdiagram/50_Logisk_diagram.png)

### Fysisk diagram

![50_Fysisk_diagram](../../images/networksec/uge07/50_netværksdiagram/50_Fysisk_diagram.png)
