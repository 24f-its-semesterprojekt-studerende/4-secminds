---
 hide:
#   - footer
---

![OSI_Model](../../images/networksec/uge07/11_OSI_Model/osi_model.png)

## Instruktioner

### 1) Gennemfør i fællesskab tryhackme rummet OSI model

#### Task 1: What is the OSI Model?

![OSI](../../images/networksec/uge07/11_OSI_Model/osi.png)

#### Task 2: Layer 7 - Application

![Application](../../images/networksec/uge07/11_OSI_Model/application.png)

#### Task 3: Layer 6 - Presentation

![Presentation](../../images/networksec/uge07/11_OSI_Model/presentation.png)

#### Task 4: Layer 5 - Session

![Session](../../images/networksec/uge07/11_OSI_Model/session.png)

#### Task 5: Layer 4 - Transport

![Transport](../../images/networksec/uge07/11_OSI_Model/transport1.png)
![Transport](../../images/networksec/uge07/11_OSI_Model/transport2.png)

#### Task 6: Layer 3 - Network

![Network](../../images/networksec/uge07/11_OSI_Model/network.png)

#### Task 7: Layer 2 - Data Link

![Data_Link](../../images/networksec/uge07/11_OSI_Model/data_link.png)

#### Task 8: Layer 1 - Physical

![Physical](../../images/networksec/uge07/11_OSI_Model/physical.png)

#### Task 9: Practical - OSI Game

Sjovt!

![Game](../../images/networksec/uge07/11_OSI_Model/game.png)

### 2) Find almindelige (consumer) eksempler på hardware enheder, og placer dem på OSI modellen.

- Smart TV (7)
- Router (3)
- Switch (2)
- Medie (1)

### 3) Find nye/sjældne/esoteriske enheder (fysiske eller virtuelle) (med referencer) - f.eks. next-gen firewalls, proxies, application gateways. Hvad-som-helst med et netstik/wifi er ok at have med.