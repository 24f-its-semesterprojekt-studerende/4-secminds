---
 hide:
#   - footer
---

## Instruktioner
Gennemgå dokumentationen for opnsense på https://docs.opnsense.org/ samtidig med at i finder tingene på jeres opnsense installation i proxmox.

1) Hvilke features tilbyder opnsense?

- OPNsense includes most of the features available in expensive commercial firewalls, and more in many cases. It brings the rich feature set of commercial offerings with the benefits of open and verifiable sources.
- Interfaces
- Firewall
- VPN
- 3rd party plugins
- Med flere.

2) Hvordan kan du kontrollere om din opnsense version har kendte sårbarheder?

- Security -> Staying Ahead -> OPNsense har integreret sikkerhedscheck.

3) Hvad er lobby og hvad kan man gøre derfra?

- Dashboard, skift password, slut session, osv.

4) Kan man se netflow data direkte i opnsense og i så fald hvor kan man se det?

- Network traffic under summary

5) Hvad kan man se omkring trafik i Reporting?

- Trafik monitorering, som viser nuværende data forbrug.

6) Hvor oprettes vlan's og hvilken IEEE standard anvendes?

- System -> Other Types -> VLAN
- IEEE 802.1Q

7) Er der konfigureret nogle firewall regler for jeres interfaces (LAN og WAN)? Hvis ja, hvilke?

- Nej

8) Hvilke slags VPN understøtter opnsense?

- IPsec
- OpenVPN
- Wireguard
- Plugin VPN options?

9) Hvilket IDS indeholder opnsense?

- Suricata

10) Hvor kan man installere os-theme-cicada i opnsense?

- Download  theme som iso fil og køres direkte

11) Hvordan opdaterer man firmware på opnsense?

- System -> Firmware -> Status -> Check for updates

12) Hvad er nyeste version af opnsense?

- 24.1.1 (Feb 6 2024)

13) Er jeres opnsense nyeste version? Hvis ikke så opdater den til nyeste version :-)

![Update](../../images/networksec/uge07/23_opnsense_udforskning/opnsense_update.png)