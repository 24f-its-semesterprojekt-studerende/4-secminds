---
 hide:
#   - footer
---

## Instruktioner

### Task 1 - Introduction

![1](../../images/networksec/uge07/40_Nmap/1.png)

### Task 2 - TCP and UDP Ports

![2](../../images/networksec/uge07/40_Nmap/2.png)

### Task 3 - TCP Flags

![3](../../images/networksec/uge07/40_Nmap/3.png)

### Task 4 - TCP Connect Scan

![4.1](../../images/networksec/uge07/40_Nmap/4.1.png)

![4.2](../../images/networksec/uge07/40_Nmap/4.2.png)

### Task 5 - TCP SYN Scan

![5.1](../../images/networksec/uge07/40_Nmap/5.1.png)

![5.2](../../images/networksec/uge07/40_Nmap/5.2.png)

### Task 6 - UDP Scan

![6](../../images/networksec/uge07/40_Nmap/6.png)

### Task 7 - Fine-Tuning Scope and Performance

![7](../../images/networksec/uge07/40_Nmap/7.png)

### Task 8 - Summary

Nmap er et dybdegående port-scanningsværktøj.

![8](../../images/networksec/uge07/40_Nmap/8.png)

