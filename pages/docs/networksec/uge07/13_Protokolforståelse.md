---
 hide:
#   - footer
---

## Instruktioner
1) I kali åbn wireshark og lyt på eth0 interfacet indtil i har sniffet en god mængde trafik. Hvis der ikke kommer meget trafik kan i åbne browseren på kali og browse et par sider, så burde der komme en del trafik.


2) Tryk på stop knappen i wireshark (den røde firkant)


3) Gem trafikken som en .pcapng fil på kali maskinen i documents mappen


4) Find et TCP 3-way handshake i trafikken og sammenlign wireshark dataen med beskrivelsen i RFC9293 afsnit 3.5 og se om i kan se en sammenhæng?

![RFC_TCP](../../images/networksec/uge07/11_OSI_Model/rfc3_5.png)

5) Hvordan er tcp headeren opbygget? Brug jeres wireshark trafik og RFC9293 til at undersøge det.

![TCP_HEADER](../../images/networksec/uge07/11_OSI_Model/tcp-header.png)

6) Læs om TLS handshake


7) Find et TLS handshake i jeres wireshark trafik og identificer Client Hello 

- Hvor mange cipher suites understøtter klienten?

34

- Hvilke TLS versioner understøtter klienten?

![CLIENT_HELLO](../../images/networksec/uge07/11_OSI_Model/client_hello.png)

8) Identificer Server hello pakken.

- Hvilken cipher suite vælger serveren?

TLS_AES_256_GCM_SHA384

- Hvilken TLS version vælger serveren?

TLS 1.3

![SERVER_HELLO](../../images/networksec/uge07/11_OSI_Model/server_hello.png)