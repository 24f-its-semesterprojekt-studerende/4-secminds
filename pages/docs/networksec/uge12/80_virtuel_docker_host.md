---
hide:
  - footer
---

# Øvelse 80 - Virtuel Docker host 

## Information

Dette er en gruppeøvelse.  

I denne øvelse skal i udvide jeres proxmox installation med en virtuel maskine der har docker installeret.
Jeg anbefaler i bruger Ubuntu server uden desktop installeret.

Herunder kan i se at docker hosten skal være på `MONITOR` subnettet.

![Lab diagram 20240319 - NISI](../../images/networksec/uge12/80_virtual_docker_host/lab_diagram_20240319.png)

## Instruktioner

1) Lav en ny VM i proxmox og konfigurer den som vist herunder:

![Proxmox docker vm config](../../images/networksec/uge12/80_virtual_docker_host/proxmox_docker_vm_config.png)

2) Brug MAC adressen fra den virtuelle maskine til at lave en statisk lease i DHCP på `MONITOR` netværket i opnsense. 
  
![Proxmx docker ip](../../images/networksec/uge12/80_virtual_docker_host/proxmox_docker_vm_ip.png)

3) Installer docker via denne guide [https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)

4) Dokumenter opsætning af ip og docker på gitlab.

- Setup af Docker-Host Ubuntu Server på proxmox

![docker-host-setup](../../images/networksec/uge12/80_virtual_docker_host/docker-host-setup.png)

- Setup af service for Docker-Host

![docker-host-service](../../images/networksec/uge12/80_virtual_docker_host/docker-host-service.png)

![docker-host-static](../../images/networksec/uge12/80_virtual_docker_host/docker-host-static.png)

- [Step 1 - Installing Docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04) følges og der checkes bagefter, at docker er installeret og er aktivt

![docker-active](../../images/networksec/uge12/80_virtual_docker_host/docker-active.png)


## Ressourcer

- [Docker dokumentation](https://docs.docker.com/)





