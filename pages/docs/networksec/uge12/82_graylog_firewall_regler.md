---
hide:
  - footer
---

# Øvelse 82 - Graylog og firewall regler 

## Information

Dette er en gruppeøvelse. Øvelsen forudsætter at i har løst Øvelse 81.

Det er nødvendigt at tillade trafik fra opnsense på `MANAGEMENT` netværket til graylog på `MONITOR` netværket.
Dette for at tillade den data (syslog+netflow) der skal sendes fra opnsense til graylog og for at få adgang til webinterfacet på graylog.

## Instruktioner

1) Lav et firewall alias med hhv. port 514 (syslog) , port 2250 (netflow) og port 9100 (webinterface)  

![graylog_fw_alias](../../images/networksec/uge12/82_graylog_firewall_regler/opnsense_graylog_fw_alias.png)

2) Lav en firewall regel der tillader trafikken

![graylog_fw_log_rules](../../images/networksec/uge12/82_graylog_firewall_regler/opnsense_graylog_fw_log_rules.png)

- Setup Graylog ports

![graylog-ports](../../images/networksec/uge12/82_graylog_firewall_regler/graylog_ports.png)

- Firewall regel for MGMT

![graylog-firewall](../../images/networksec/uge12/82_graylog_firewall_regler/graylog_firewall.png)

## Ressourcer
