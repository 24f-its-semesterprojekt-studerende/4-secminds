---
 hide:
#  - footer
---

# Læringslog uge 08 - CIS Benchmarks og Netværksdesign

## Emner

- CIS Kontroller
- Ubuntu CIS Benchmarks
- Mitre ATT&CK

- Netværksdesign
- Segmentering
- Firewalls

## Mål for ugen

### Praktiske mål

- Øvelse 47 - CIS Kontroller
- Øvelse 48 - Ubuntu CIS Benchmarks
- Øvelse 49 - Mitre ATT&CK

- Øvelse 41 - Nmap Wireshark
- Øvelse 51 - Netværksdesign
- Øvelse 52 - Praktisk Netværksegmentering
- Øvelse 60 - Firewalls
- Øvelse 61 - Konfiguration af Firewalls


- Den studerende forstår hvad sikkerheds benchmarks kan bruges til
- Den studerende forstår hvad CIS benchmarks kan bruges til
- Den studerende forstår samehængen mellem CIS benchmarks og CIS Controls
- Den studerende forstår hvad Mitre ATT&CK databasen kan anvendes til.

### Læringsmål

**Viden**

- Generelle governance principper / sikkerhedsprocedure
- Relevante it-trusler

- Sikkerhed i TCP/IP
- Hvilke enheder, der anvender hvilke protokoller
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)

**Færdigheder**

- Den studerende kan følge et benchmark til at sikre opsætning af enheder.

- Identificere sårbarheder som et netværk kan have
- Den studerende kan håndtere udviklingsorienterede situationer herunder
- Designe, konstruere og implementere samt teste et sikkert netværk

**Kompetencer**

## Reflektioner over hvad vi har lært

- MITRE ATT&CK documentationen er dybdegående og nem at navigere
- Ubuntu CIS Benchmarks er et godt værktøj til automatisation af sikkerheden i Ubuntu styresystem
- CIS Kontrollerne er et godt udgangspunkt for implementeringen af sikkerhed i en virksomhed ud fra dens implementeringsgruppe-niveau

## Andet
