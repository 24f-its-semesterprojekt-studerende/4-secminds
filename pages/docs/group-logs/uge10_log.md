---
 hide:
#  - footer
---

# Læringslog uge 10 - Fysisk sikkerhed, Firewalls(regler), Netværk logs og OPNsense

## Emner

- Fysisk sikkerhed
- Host-based firewall
- Forskellige typer af netværks logs
- Implementering af netværk
- Opsætning og test af firewall regler
- Opsætning af usikre webservices
- Dokumentation af det implementerede

## Mål for ugen

### Praktiske mål

- Øvelse 19 - Installation af IP Tables
- Øvelse 20 - Bloker alt indgående trafik
- Øvelse 21 - Tillad indgående trafik fra etableret forbindelse
- Øvelse 23 - Tillad indgående trafik fra specifikke ICMP-beskeder
- Øvelse 25 - Tillad indgående trafik fra en specifik IP-adresse
- Øvelse 25.1 Eftermiddag - Opsætning af Wazuh-server

- Øvelse 25 - OPNsense Lokal Overvågning
- Øvelse 54 - Implementering af Netværksdesign

- En forståelse for at hændelser i den fysiske verden også kan udgører en it-sikkerheds trussel.
- At den studerende kan lave en grundlæggende opsætning af en firewall.
- At den studerende har forståelse for firewall'ens rolle på en host.

### Læringsmål

**Viden**

- Generelle sikkerhedprocedurer.
- Relevante it-trusler.
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI).

**Færdigheder**

- Udnytte modforanstaltninger til sikring af systemer.
- Identificere sårbarheder som et netværk kan have.
- Den studerende kan arbejde struktureret med implementering, test og dokumentation af sikrede netværk.
- Den studerende kan implementere firewall regler der sikrer adskillelse af netværkssegmenter.

**Kompetencer**

- Håndtere værktøjer til at fjerne forskellige typer af endpoint trusler.
- Håndtere udvælgelse og anvend af praktiske til at forhindre it-sikkerhedsmæssige hændelser.
- Designe, konstruere og implementere samt teste et sikkert netværk.

## Reflektioner over hvad vi har lært
- IPTables kan hurtigt blive uoverskueligt, så det bruger vi nok ikke til vores semesterprojekt.
- Schæferhunde bider hårdt.
- UCL kunne godt forbedre deres fysiske sikkerhed.
- Allow List er et godt udgangspunkt for din sikre Firewall.
- Block List fungerer godt til din public API.

- Netværksdiagrammer giver et godt overblik
- OPNsense GUI Firewall Regler er et dejligt værktøj i forhold til CLI IPTables.

## Andet