---
 hide:
#  - footer
---

# Læringslog uge 15 - SIEM, ARP, DNS, Man In The Middle

## Emner

- Introduktion til SIEM systemer.
- Introduktion til Wazuh.
- Address Resolution Protocol (ARP)
- Domain Name System (DNS)
- Man In The Middle (MITM)

## Mål for ugen

### Praktiske mål

- Øvelse 33 Overvågning Wazuh
- Øvelse 34 Detekter SQL Angreb
- Øvelse 35 Detekter Shellshock Angreb
- Øvelse 36 Detekter Ondsindede Kommandoer
- Øvelse 37 Wazuh Scan


- Øvelse 91 Grundlæggende ARP
- Øvelse 92 ARP spoofing med Ettercap
- Øvelse 93 Grundlæggende DNS
- Øvelse 94 Rekognosering
- Øvelse 95 Ettercap DNS MITM
- Øvelse 96 Tryhackme digdug

### Læringsmål

**Viden**

- Viden om relevante it-trusler
- Netværkstrusler
- Forskellige sniffing strategier og teknikker

**Færdigheder**

- Kan implementerer systematisk logning og monitering af enheder
- Kan analyser logs for hændelser og følge et revision spor
- Identificere sårbarheder som et netværk kan have

**Kompetencer**

- Kan håndtere enheder på command line-niveau
- Designe, konstruere og implementere samt teste et sikkert netværk

## Reflektioner over hvad vi har lært


## Andet

- At hver studerende har grundlæggende forståelse for hvad et SIEM system er.
- At hver studerende har lavet simple filtering af sikkerheds hændelser i Wazuh
- Den studerende kan forklare princippet bag MITM og spoofing
- Den studerende kan udføre ARP poisoning/spoofing og DNS MITM med henblik på at teste et netværks sikkerhed