---
 hide:
#  - footer
---

# Læringslog uge 09 - Linux bruger system og IPAM/OPNsense

## Emner
- Bruger systemer og rettigheder i Linux
- Dokumentation af netværk (IPAM m.m.)
- Beskyttelse af netværksenheder (CIS18 benchmarks)
- Designe netværk (træne at designe netværk med sikkerhed)

## Mål for ugen

### Praktiske mål

- Øvelse 09 - Brugerkontoer i Linux
- Øvelse 09.1 - Brugerkonto filer i Linux
- Øvelse 10 - Brugerrettigheder i Linux
- Øvelse 11 - Brugerkontoer og svage passwords
- Eftermiddag - Opsætning af Ubuntu Server på Proxmox

- Øvelse 70 IPAM med netbox
- Øvelse 24 OPNsense hærdning
- Øvelse 53 Designe sikkert netværk


- Den studerende kan oprette, slette eller ændre en bruger i bash shell
- Den studerende kan ændre bruger rettighederne i bash shell
- Den studerende har en grundlæggende forståelse for principle of privilege of least
- Den studerende har en grundlæggende forståelse for mandatory og discretionary access control

### Læringsmål

**Viden**

- Relevante it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- OS roller ift. sikkerhedsovervejelser
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)

**Færdigheder**

- Udnytte modforanstaltninger til sikring af systemer
- Identificere sårbarheder som et netværk kan have

**Kompetencer**

- Håndtere enheder på command line-niveau
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre it-sikkerhedsmæssige hændelser
- Designe, konstruere og implementere samt teste et sikkert netværk

## Reflektioner over hvad vi har lært
- NetBox var overvældende, det ville være bedre, hvis det blev introduceret i starten af lektionen, så der kunne blive stillet spørgsmål.
- Hashing virker godt - men man skal gennemtænke hvilken algoritme man bruger!
- CIS kontroller er nemt at lave, men trals at lave manuelt. Automatisér!

## Andet