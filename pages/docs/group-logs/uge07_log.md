---
 hide:
#  - footer
---

# Læringslog uge 07 - Intro til Linux, OSI og netværksprotokoller

## Emner

- Introduktion til operativsystemer
- Navigation i Linux
- OSI Modellen
- Netværksenheder
- Protokoller

## Mål for ugen

### Praktiske mål

- Øvelse 03 - Navigation i Linux filesystem
- Øvelse 04 - Linux file struktur
- Øvelse 06 - Søgning i Linux file strukturen
- Øvelse 07 - Ændring og søgning i filer

- Øvelse 11 - OSI Modellen
- Øvelse 12 - OSI Modellen og Protokoller
- Øvelse 13 - Wireshark
- Øvelse 23 - OPNsense udforskning
- Øvelse 40 - TryHackMe Nmap
- Øvelse 50 - Netværksdiagram

### Læringsmål

**Viden:** 

- Relevante it-trusler (Oplæg)
- Relevante sikkerhedsprincipper til systemsikkerhed

- Hvilke enheder, der anvender hvilke protokoller
- Forskellige sniffing strategier og teknikker
- Adressering i de forskellige lag
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI).
- Netværkstrusler

**Færdigheder:**

- Identificere sårbarheder som et netværk kan have

**Kompetencer:**

- Den studerende kan håndtere enheder på command line-niveau
- Monitorere og administrere et netværks komponenter

## Reflektioner over hvad vi har lært

- Brug af portscanningsværktøjet Nmap
- Forståelse for OSI Modellen
- Anvendelse af Linux CLI
- Grundlæggende netværksinfrastruktur

## Andet