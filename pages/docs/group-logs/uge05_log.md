---
 hide:
#  - footer
---

# Læringslog uge 05 - Introduktion til gruppen og Proxmox

## Emner
- Team building
- Proxmox opsættelse

## Mål for ugen

### Praktiske mål

- Sagt hej
- Lært at sætte ram/harddisk i en computer
- Udført Proxmox opsætning

### Læringsmål

- Forståelse af læringsmål
- Hvad semesteret består af.

## Reflektioner over hvad vi har lært

- Proxmox kan ikke lide bindestreger i hostname.

## Andet
[Hardware/Proxmox](../networksec/uge06/proxmox_setup.md)