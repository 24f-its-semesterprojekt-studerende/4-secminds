---
 hide:
#  - footer
---

# Læringslog uge 06 - Introduktion til fagene

## Emner

- Introduktion til faget System sikkerhed
- Opsætning af ubuntu server
- Introduktion til faget Netværk- og kommunikationssikkerhed.
- Grundlæggende netværk.
- Opsætning af værktøjer.

## Mål for ugen

### Praktiske mål
- Øvelse 00 - Læringsmål
- Øvelse 10 - Grundlæggende netværksviden
- Øvelse 20 - OPNsense på VMWare
- Øvelse 21 - OPNsense på proxmox
- Øvelse 22 - OPNsense viden
- Øvelse 30 - Kali Linux på proxmox

Alle studerende har forståelse for fagets formål.
Alle studerende kan (Nogenlunde)tolke læringsmålene for faget.
Alle studerende har en fungerende Ubuntu Server VM.

### Læringsmål

*I denne uge arbejdede vi ikke med konkrette læringsmål fra studieordningen, men forståelsen for dem*

## Reflektioner over hvad vi har lært

- Hvordan læringsmålene skal forstås
- Semesterstrukturen

## Andet