---
 hide:
#  - footer
---

# Læringslog uge 12 - Logs, netværkstrafik, graylog

## Emner

- Wordlist skanning
- Implementering af docker vm host
- Opsætning af portainer på docker vm host
- Opsætning og konfiguration af graylog
- Dokumentation af det implementerede

## Mål for ugen

### Praktiske mål

- Øvelse 42 Enumering med offensive værktøjer
- Øvelse 80 Virtuel Docker host
- Øvelse 81 Portainer på Docker host
- Øvelse 82 Graylog og firewall regler
- Øvelse 83 Graylog installation i docker via Portainer
- Øvelse 84 Graylog konfiguration

### Læringsmål

**Viden**

- Netværkstrusler
- Forskellige sniffing strategier og teknikker
- Netværk management (overvågning/logning, snmp)

**Færdigheder**

- Overvåge netværk samt netværkskomponenter
- Teste netværk for angreb rettet mod de mest anvendte protokoller

**Kompetencer**

- Designe, konstruere og implementere samt teste et sikkert netværk
- Monitorere og administrere et netværks komponenter

## Reflektioner over hvad vi har lært


## Andet

- Den studerende har en forståelse for hvordan logning af netværks data kan udføres
- Den studerende kan opsætte og konfigurere et logopsamlingssystem
- Den studerende kan udføre wordlist skanninger med henblik på at afprøve logopsamlingssystemet