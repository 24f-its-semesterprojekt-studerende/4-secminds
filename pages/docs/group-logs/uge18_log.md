---
 hide:
#  - footer
---

# Læringslog uge 18 - Red/Blue/Purple Teaming

## Emner

- Red teaming
- Blue teaming
- Purple teaming
- Cyber kill chains

## Mål for ugen

### Praktiske mål

- Øvelse 110 Red Teaming
- Øvelse 110 Blue Teaming
- Øvelse 112 Purple Teaming

### Læringsmål

**Viden**

- Netværkstrusler

**Færdigheder**

- Teste netværk for angreb rettet mod de mest anvendte protokoller.
- Identificere sårbarheder som et netværk kan have.

**Kompetencer**

- Designe, konstruere og implementere samt teste et sikkert netværk.
- Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report)

## Reflektioner over hvad vi har lært


## Andet

- Den studerende har viden om red, blue og purple teaming
- Den studerende har viden om cyber kill chains
- Den studerende kan planlægge sikkerhedstest af netværk
- Den studerende kan rapportere om sikkerhedstest af netværk