---
 hide:
#  - footer
---

# Læringslog uge 19 - Semesterprojekt 1

## Emner

- Intro til semesterprojekt

## Mål for ugen

### Praktiske mål

- Problemformulering
- Forståelse for formalia
- Setup for projekt gitlab dokumentation
- Aftale af projektstruktur

### Læringsmål

*Der er ikke blevet arbejdet med specfikke læringsmål denne uge*

## Reflektioner over hvad vi har lært

- Hvad kravene for semesterprojektet er :) 

## Andet
