---
 hide:
#  - footer
---

# Læringslog uge 11 - Logning i Linux

## Emner

- Logning

## Mål for ugen

### Praktiske mål

- Øvelse 12 - Linux log system
- Øvelse 25.2 Eftermiddag - Opsætning af Wazuh agent

- Den studerende kan redegøre for hvornår der som minimum bør laves en log linje
- Den studerende har grundlæggende forståelse for log management

### Læringsmål

**Viden**

- Generelle governance principper
- Relevante it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed

**Færdigheder**

- Implementere systematisk logning og monitering af enheder (Påbegyndt)
- Analyser logs for incidents og følge et revisionsspor (Påbegyndt)

**Kompetencer**

- Håndtere udvælgelse af praktiske mekanismer til at detektere it-sikkerhedsmæssige hændelser.

## Reflektioner over hvad vi har lært


## Andet