---
 hide:
#   - footer
---

# Dokumentation-Skabelon

## Information

- Hvad er dokumenteret, det vil sige kontekst for denne dokumentation

## Instruktioner

- trin for trin guide
- Skrives så det udførte kan reproduceres af andre
- Relevante screenshots

## Links

- Links til kode
- Links til relevante online ressourcer