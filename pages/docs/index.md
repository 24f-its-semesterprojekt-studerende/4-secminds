---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Semesterprojekt gruppe 4 - Secminds

#### Denne gruppe består af:
- Kenneth Haahr (2. Semester - Datamatiker)

- Rune Toftlund (1. Semester - IT-teknolog)

- Frederik Nilsson (2. Semester - Tidligere DTU)

På dette website finder du øvelser, projektplaner, læringslogs og andet relateret til semesterprojektet :)

Læringslogs og link til dokumentation skal inkluderes som bilag i projektrapporten (eksamensaflevering).  

Procesafsnittet i rapporten skal beskrive de enkelte ugers projektarbejde med henvisning til logs (link til relevante dele af denne gitlab side) 

Semesterprojektet er beskrevet på: [https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/](https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/)