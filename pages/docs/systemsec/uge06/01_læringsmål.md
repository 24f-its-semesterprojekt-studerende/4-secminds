---
 hide:
#   - footer
---

### Viden
- Generelle governance principper / sikkerhedsprocedurer
    - NIS/CIS-kontroller, OSI model
- Væsentlige forensic processer
    - Analyseværktøjer? Wireshark? Scanning, Logs
- Relevante it-trusler
    - OWASP Top10, følg med i nyhederne
- Relevante sikkerhedsprincipper til systemsikkerhed
    - Defense in Depth, Secure by Design
- OS roller ift. Sikkerhedsovervejelser
    - Kompartmentalisering, admin/user/guest, iterativ process
- Sikkerhedsadministration i DBMS.
    - Autentisering og autorisation
### Færdigheder
- Udnytte modforanstaltninger til sikring af systemer
    - Kompartmentalisering / opsætning af netværk / defense in depth, ISO27000. Firewall, autentisering/autorisation
- Følge et benchmark til at sikre opsætning af enhederne
    - CIS-Kontroller, NIS
- Implementere systematisk logning og monitering af enheder
    - Logger software implementering, automatisation, bash/powershell
- Analysere logs for incidents og følge et revisionsspor
    - Værktøjer; Logning/Wireshark
- Kan genoprette systemer efter en hændelse.
    - Backup, roll back? Netværksopsætning.
### Kompetencer
- Håndtere enheder på command line-niveau
    - Linux/Bash/PowerShell
- Håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
    - Nmap, Wireshark, API opsætning, BurpSuite.
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser.
    - Anvendelse af værktøjer: Wireshark, Nmap. Netværksopsætning.
- Håndtere relevante krypteringstiltag
    - Hashing, RSA/AES, TLS (1.3)
