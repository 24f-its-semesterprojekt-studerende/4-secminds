## Information
I forrige uge opsatte i en Wazuh-server. Denne uge skal I opsætte en Wazuh-agent på den Ubuntu-instans, der **ikke** fungerer som vært for Wazuh-serveren, altså den Ubuntu-VM på Proxmox med navnet _Target host_.

Wazuh overvåger som udgangspunkt et system ved at analysere linjer fra logfiler på det enkelte system. For at Wazuh kan modtage loglinjer fra et system, skal der være en applikation på det overvågede system, som sender disse loglinjer til Wazuh. Denne type applikation kaldes i Wazuh-domænet en agent.

Helt grundlæggende skaber log filer i sig selv ikke værdi eller øger sikkerheden, hvis der ikke er nogen som
læser eller analyser dem. SIEM systemer såsom Wazuh kan automatisere log analyse for os, og sender alarmer ved
specifikke mønstre eller begivenheder i et en log file.

I denne opgave skal i lave en opsætning af en Wazuh agent på Ubuntu instansen _Target host_ og valider at
agenten forbinder til den Wazuh server i satte op i forrige uge. Herefter skal i prøve at udløse en alarm ved at tilføje en ny bruger på den overvåget Ubuntu instanse.

Formålet med øvelsen er at der skabes en grundlægende forståelse for hvordan Wazuh agenter fungerer, samt hvordan
et SIEM system anvender log filer.

## Instruktioner
  
### Opsætning og afprøving af Wazuh agent.
1) Lav opsætningen af Wazuh agenten på _Target host_ ved følge [dokumenationen](https://documentation.wazuh.com/current/installation-guide/wazuh-agent/wazuh-agent-package-linux.html)
_Den sidste del med 'Disable Wazuh update' behøver i ikke at følge_

2) Verificer at Wazuh agent har forbindeles til Wazuh server ved at følge guiden i [afsnittet Using the Wazuh dashboard](https://documentation.wazuh.com/current/user-manual/agents/agent-connection.html#checking-connection-with-the-wazuh-manager)

- Følgende kommando køres fra host vm, således den bliver en agent

![setup-agent](../../images/systemsec/uge11/setup-agent.png)

- Den startes

![start-agent](../../images/systemsec/uge11/start-agent.png)

- Dashboarded viser vores agent(er)

![agents](../../images/systemsec/uge11/agents.png)

3) På den overvåget Ubuntu instansen _Target host_, tilføj en ny bruger med navnet `darth`.

- Darth bruger tilføjes

![darth](../../images/systemsec/uge11/useradded.png)

4) Log ind på Wazuh dashboard, og klik ind på _Security events_

5) I _Security events_ under _Security alerts_, Valider at Wazuh har detekteret en begivenhed der falder under Mitre teknikken T1136, i forbindelse med at den nye bruger blev oprettet

- Wazuh detekterer en begivenhed, som falder under Mitre teknikken T1136

![security-events](../../images/systemsec/uge11/security-event.png)

6) Udvid informationen på alarmen der blev udløst i forbindelse med oprettelsen af den nye bruger ved at klikke på pilen til venstre for alarm linjen, og se om i kan identificere hvilken log file wazuh agenten læste fra(Feltet med information hedder location)

![location](../../images/systemsec/uge11/location.png)

7) Undersøg hvor meget Information i kan finde om den nye bruger i alarmen.

- Der er rigtig... rigtig... meget information :)

```
{ 
    "predecoder": { "hostname": "host", "program_name": "useradd", "timestamp": "Mar 21 18:42:11" }, 
    "agent": {"ip": "10.34.20.103", "name": "host", "id": "001" }, 
    "manager": { "name": "wazuh" }, 
    "data": { "uid": "1001", "gid": "1001", "shell": "/bin/sh,", "dstuser": "darth", "home": "/home/darth" }, 
    "rule": { "mail": false, "level": 8, "pci_dss": [ "10.2.7", "10.2.5", "8.1.2" ], "hipaa": [ "164.312.b", "164.312.a.2.I", "164.312.a.2.II" ], "tsc": [ "CC6.8", "CC7.2", "CC7.3" ], "description": "New user added to the system.", "groups": [ "syslog", "adduser" ], "nist_800_53": [ "AU.14", "AC.7", "AC.2", "IA.4" ], "gdpr": [ "IV_35.7.d", "IV_32.2" ], "firedtimes": 1, "mitre": { "technique": [ "Create Account" ], "id": [ "T1136" ], "tactic": [ "Persistence" ] }, "id": "5902", "gpg13": [ "4.13" ] }, 
    "decoder": { "parent": "useradd", "name": "useradd" }, 
    "full_log": "Mar 21 18:42:11 host useradd[4138]: new user: name=darth, UID=1001, GID=1001, home=/home/darth, shell=/bin/sh, from=/dev/pts/0", 
    "input": { "type": "log" }, 
    "@timestamp": "2024-03-21T18:42:13.841Z", "location": "/var/log/auth.log", "id": "1711046533.1315286", "timestamp": "2024-03-21T18:42:13.841+0000", 
    "_id": "ANhSYo4Bxz-1PuLyKcat" 
}
```

## Links
[Setting up a wazuh agent](https://documentation.wazuh.com/current/installation-guide/wazuh-agent/wazuh-agent-package-linux.html)
