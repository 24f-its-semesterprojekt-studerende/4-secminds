---
 hide:
#   - footer
---

## Instruktioner
1) Download CIS Ubuntu Linux-benchmark-dokumentet fra It's learning (under ressourcer til dagens lektion).


2) Skim afsnittet "Overview", og læg mærke til, at alle handlinger skal udføres som root-brugeren (og ikke med sudo-bruger).


3) Udvælg en eller flere benchmarks at arbejde med.


4) Brug en af gruppens medlemmers Ubuntu-serverinstans til at gennemgå de udvalgte benchmarks fra CIS-benchmarken.


5) For hvert punkt i benchmarken skal I undersøge, om systemet overholder anbefalingerne (under punktet "audit").


6) Identificer eventuelle åbenlyse afvigelser fra anbefalingerne.


7) Identificer eventuelle foranstaltninger og implementer dem.


<hr style="height:2px;border-width:0;color:gray;background-color:gray">

## Der tages udgangspunkt i 1.6.1 Configure AppArmor

### 1.6.1.1 Ensure AppArmor is installed (Automated)
#### Profile Applicability
- Level 1 - Server
- Level 1 - Workstation

#### Description
AppArmor provides Mandatory Access Controls.

#### Rationale
Without a Mandatory Access Control system installed only the default Discretionary
Access Control system will be available.

#### Audit
Verify that AppArmor is installed:

<code> dpkg-query -W -f='${binary:Package}\t${Status}\t${db:Status-Status}\n'apparmor<br>
</code>

![1.6.1.1](../../images/systemsec/uge08/48_1.6.1.1.png)

#### Remediation
Install AppArmor.

<code> apt install apparmor </code>

#### CIS Controls

| Control | IG 1 | IG 2 | IG 3 |
| :------| :---:|:----:|:----:|
| 3.3 Configure Data Access Control Lists | x | x | x |

<hr style="height:2px;border-width:0;color:gray;background-color:gray">

### 1.6.1.2 Ensure AppArmor is enabled in the bootloader configuration (Automated)

#### Profile Applicability
- Level 1 - Server
- Level 1 - Workstation

#### Description
Configure AppArmor to be enabled at boot time and verify that it has not been
overwritten by the bootloader boot parameters.
Note: This recommendation is designed around the grub bootloader, if LILO or another
bootloader is in use in your environment enact equivalent settings.

#### Rationale
AppArmor must be enabled at boot time in your bootloader configuration to ensure that
the controls it provides are not overridden.

#### Audit
Run the following commands to verify that all linux lines have the apparmor=1 and
security=apparmor parameters set:

<code>
\# grep "^\s*linux" /boot/grub/grub.cfg | grep -v "apparmor=1"<br>
Nothing should be returned<br>
\# grep "^\s*linux" /boot/grub/grub.cfg | grep -v "security=apparmor"<br>
Nothing should be returned
</code>

![1.6.1.2](../../images/systemsec/uge08/48_1.6.1.2.png)

#### Remediation
Edit /etc/default/grub and add the apparmor=1 and security=apparmor parameters to
the GRUB_CMDLINE_LINUX= line

<code>
GRUB_CMDLINE_LINUX="apparmor=1 security=apparmor"
</code>

Run the following command to update the grub2 configuration:

<code>
\# update-grub
</code>

#### CIS Controls

| Control | IG 1 | IG 2 | IG 3 |
| :------| :---:|:----:|:----:|
| 3.3 Configure Data Access Control Lists | x | x | x |

#### MITRE ATT&CK Mappings

| Techniques / Sub Techniques | Tactics | Mitigations |
| :------| :---:|:----:|
| T1068, T1068.000, T1565, T1565.001, T1565.003 | TA0003 | M1026 |

<hr style="height:2px;border-width:0;color:gray;background-color:gray">

### 1.6.1.3 Ensure all AppArmor Profiles are in enforce or complain mode (Automated)
#### Profile Applicability
- Level 1 - Server
- Level 1 - Workstation

#### Description:
AppArmor profiles define what resources applications are able to access.

#### Rationale
Security configuration requirements vary from site to site. Some sites may mandate a
policy that is stricter than the default policy, which is perfectly acceptable. This item is
intended to ensure that any policies that exist on the system are activated.

#### Audit
Run the following command and verify that profiles are loaded, and are in either enforce
or complain mode:

<code>
\# apparmor_status | grep profiles
</code>

Review output and ensure that profiles are loaded, and in either enforce or complain
mode:

<code>
37 profiles are loaded. <br>
35 profiles are in enforce mode. <br>
2 profiles are in complain mode. <br>
4 processes have profiles defined. <br>
</code>

Run the following command and verify no processes are unconfined

<code>
\# apparmor_status | grep processes
</code>

Review the output and ensure no processes are unconfined:

<code> 
4 processes have profiles defined. <br>
4 processes are in enforce mode. <br>
0 processes are in complain mode. <br>
0 processes are unconfined but have a profile defined. <br>
</code>

![1.6.1.3](../../images/systemsec/uge08/48_1.6.1.3.png)

#### Remediation
Run the following command to set all profiles to enforce mode:

<code>
\# aa-enforce /etc/apparmor.d/*
</code>

OR
Run the following command to set all profiles to complain mode:

<code>
\# aa-complain /etc/apparmor.d/*
</code>

**Note: Any unconfined processes may need to have a profile created or activated for them and then be restarted**

#### CIS Controls

| Control | IG 1 | IG 2 | IG 3 |
| :------| :---:|:----:|:----:|
| 3.3 Configure Data Access Control Lists | x | x | x |

#### MITRE ATT&CK Mappings

| Techniques / Sub Techniques | Tactics | Mitigations |
| :------| :---:|:----:|
|  | TA0005 |  |

<hr style="height:2px;border-width:0;color:gray;background-color:gray">

### 1.6.1.4 Ensure all AppArmor Profiles are in enforce or complain mode (Automated)
#### Profile Applicability
- Level 2 - Server
- Level 2 - Workstation

#### Description
AppArmor profiles define what resources applications are able to access.

#### Rationale
Security configuration requirements vary from site to site. Some sites may mandate a
policy that is stricter than the default policy, which is perfectly acceptable. This item is
intended to ensure that any policies that exist on the system are activated.

#### Audit
Run the following command and verify that profiles are loaded, and are in either enforce
or complain mode:

<code>
\# apparmor_status | grep profiles
</code>

Review output and ensure that profiles are loaded, and in either enforce or complain
mode:

<code>
34 profiles are loaded. <br>
34 profiles are in enforce mode. <br>
0 profiles are in complain mode. <br>
2 processes have profiles defined. <br>
</code>

Run the following command and verify no processes are unconfined

<code>
\# apparmor_status | grep processes
</code>

Review the output and ensure no processes are unconfined:

<code> 
2 processes have profiles defined. <br>
2 processes are in enforce mode. <br>
0 processes are in complain mode. <br>
0 processes are unconfined but have a profile defined. <br>
</code>

![1.6.1.3](../../images/systemsec/uge08/48_1.6.1.3.png)

#### Remediation
Run the following command to set all profiles to enforce mode:

<code>
\# aa-enforce /etc/apparmor.d/*
</code>

**Note: Any unconfined processes may need to have a profile created or activated for them and then be restarted**

#### CIS Controls

| Control | IG 1 | IG 2 | IG 3 |
| :------| :---:|:----:|:----:|
| 3.3 Configure Data Access Control Lists | x | x | x |

#### MITRE ATT&CK Mappings

| Techniques / Sub Techniques | Tactics | Mitigations |
| :------| :---:|:----:|
| T1068, T1068.000, T1565, T1565.001, T1565.003 | TA0005 |  |