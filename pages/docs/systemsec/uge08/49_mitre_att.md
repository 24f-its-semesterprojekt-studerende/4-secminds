---
 hide:
#   - footer
---

## Information
Formålet med denne øvelse er at udforske Mitre ATT&CK-rammeværket med fokus på taktikken Privilege Escalation (Forhøjelse af Privilegier), teknikken T1548, samt afhjælpningen M1026 & Detekteringen DS0022.

## Instruktioner
#### 1) Undersøg Mitre ATT&CK taktikken TA0004. Hvad betyder denne taktik?

**TA0004: Privilege Escalation**

Trusselsaktøren prøver at opnå højere rettigheder f.eks. ved:

- SYSTEM/root level
- local administrator
- user account with admin-like access
- user accounts with access to specific system or perform specific function


#### 2) Identificer specifikke under-teknikker under T1548 og hvordan de bidrager til at opnå Privilege Escalation.

#### T1548: Abuse Elevation Control Mechanism

##### .001: Setuid and Setgid

- Midlertidig anvendelse af højere rettigheder 

##### .002: Bypass User Account Control

- Omgå det implementerede Windows User Account Control system

##### .003: Sudo and Sudo Caching

- Udføre sudo kommandoer uden sudo rettigheder pga. dårlig config.

##### .004: Elevated Execution With Prompt

- macOS AuthorizationExecuteWithPrivileges API validerer ikke at requesten kommer fra en "sikker" source eller at indholdet ikke er ondsindet

##### .005: Temporary Elevated Cloud Access

- Account Impersonation kan misbruges ved at anvende andre brugers rettigheder


#### 3) Identificer og undersøg Mitigeringen M1026.

**M1026: Privileged Account Management**

- Håndterer oprettelsen, ændringerne og rettighederne associeret til users inkl. SYSTEM og root.
- Password til root/admin så CLI ikke kan tilgås.
- Overblik over hvilke brugere eksisterer samt deres rettigheder.

#### 4) Identificer og undersøg detekteringen DS0022.

**DS0022: File**

- Data source.

## Links
- [MITRE_ATT&CK_TA0004](https://attack.mitre.org/tactics/TA0004/)
- [MITRE_ATT&CK_M1026](https://attack.mitre.org/mitigations/M1026/)
- [MITRE_ATT&CM_DS0022](https://attack.mitre.org/datasources/DS0022/)
