# Opgave 35 - Detekter forsøg på shellshock angreb

## Information
I forrige opgave (34) overvåget wazuh apache web serveren for forsøg på sql injection angreb.
Wazuh kan også "ud af boksen" detekter andre angreb. I denne opgave er det angrebet "Shellshock" som 
skal detekteres. Et shellshock angreb er egentlig bare et "Command injection" angreb. Altså man forsøger at "inject" en shell kommando ind i en applikation, i dette tilfælde gennem headeren _User-Agent_.
Forklaring på command injection angreb kan du finde [her](https://www.hacksplaining.com/signup?next=/prevention/command-execution)


Da jeg i sin tid skrev disse øvelser, var Wazuh dokumentationen meget mangelfuld. Det er den ikke længere, og en næsten
Equivalent til denne øvelse, kan findes i wazuh dokumentationen [her](https://documentation.wazuh.com/current/proof-of-concept-guide/detect-web-attack-shellshock.html).


## Instruktioner

### Opsætning af Wazuh agent
1. Installer apache og overvåg loggen (Såfremt du ikke allerede har gjort dette i opgave 34)

### Udfør angreb
1. Fra en angribende host(F.eks.), eksekver kommandoen `sudo curl -H "User-Agent: () { :; }; /bin/cat /etc/passwd" <Den overvåget host-IP>`

### Wazuh dashboard
1. Gå til Security events.  
2. filterer på: `rule.description:Shellshock attack detected`  
![Shellshock attack detected](../../images/systemsec/uge15/35/Shellshock_inject_attempt.jpg)
   
- Shellshock angreb forsøges

![shellshock_attack_cli](../../images/systemsec/uge15/35/shellshock_attack.png)

- Wazuh Dashboard detekterer angreb

![shellshock_wazuh](../../images/systemsec/uge15/35/shellshock_wazuh.png)

## Links
[Detecting a Shellshock attack](https://documentation.wazuh.com/current/proof-of-concept-guide/detect-web-attack-shellshock.html)
