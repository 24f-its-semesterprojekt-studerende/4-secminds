---
 hide:
#  - footer
---

## Information
I Linux håndteres netværksfiltreringen (firewall) med et kerne-modul, der hedder `Netfilter`.
At interagere direkte med `Netfilter`-modulet er komplekst og ineffektivt. Derfor anvendes der
typisk et interface til at interagere med `Netfilter`. Et eksempel på et sådant interface er 
iptables. I undervisningen bruger vi iptables til implementering af firewall-regler.
  
I den kommende opgave skal iptables installeres.

**I alle øvelserne kan du evt. bruge NMap til at test firewall konfigurationen, eller ved at pinge hosten fra en anden host**
  
## Instruktioner
1) Installer iptables med kommandoen `sudo apt install iptables`

2) Udskriv versionsnummeret med kommandoen `sudo iptables -V`

``iptables v1.8.9 (nf_tables)``

3) Installer iptables-persistent med kommandoen `sudo apt install iptables-persistent`

4) Udskriv alle firewall-regler (regelkæden) med kommandoen `sudo iptables -L`

![IPtables](../../images/systemsec/uge10/iptables.png)

5) Reflekter over, hvad der menes med firewall-regler, samt hvad der menes med "regelkæden".

- Den printer en Allow liste for hver kategori; Input, Output, Forward

**Når man har ændret en iptables firewall-regel, skal man manuelt gemme den nuværende regelopsætning med kommandoen `sudo netfilter-persistent save`, ellers er regellisten tom efter genstart.**

## Links