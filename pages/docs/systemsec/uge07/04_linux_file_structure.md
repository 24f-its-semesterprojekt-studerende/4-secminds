---
 hide:
#   - footer
---

## Instruktioner

 Hvert Linux-directory i roden har per konvention et specifikt formål, altså hvilken data de typisk indeholder. For eksempel bør directoryet var/log indeholde alle applikationslogs fra applikationer, der bliver eksekveret.

/ - root

|_bin - Binare programmer (executables)

|_boot - Indeholder boot konfigurationer

|_dev - Devices filer (keyboard, mouse etc)

|_etc - Konfigurationsfiler

|_home - Personlige filer

|_media - USB, DVD osv. filer ligger herinde

|_lib - Kernel moduler, C programming code library

|_mnt - Moint point til temp media

|_misc - Miscellaneous filer, eksterne komponenter

|_proc - Information om systemets processor og kernel

|_opt - Reserveret til installation af add-ons

|_sbin - Udlukkede filer kun til sudo

|_root - Systemets rod

|_tmp - Midlertidlige filer

|_usr - Delt mellem brugerne

|_var - Variabel filer, logs, tracking, caches osv.
