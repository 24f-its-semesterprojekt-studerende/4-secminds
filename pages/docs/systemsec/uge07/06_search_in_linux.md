---
 hide:
#   - footer
---

## Instruktioner

1) I Home directory, eksekver kommandoen find.

<code>Alle directories og filer under dit directory udskrives.</code>

2) I Home directory, eksekver kommandoen find /etc/.

<code>Alle directories og filer under home/etc/ udskrives.</code>

3) Eksekver kommandoen sudo find /etc/ -name passwd. Sudo foran kommandoen betyder, at kommandoen eksekveres med de rettigheder, som sudo-gruppen har.

<code>/etc/pam.d/passwd <br>
/etc/passwd</code>


4) Eksekver kommandoen sudo find /etc/ -name pasSwd. Husk stort S.

<code>Ingenting</code>

5) Eksekver kommandoen sudo find /etc/ -iname pasSwd. Husk stort S.

<code>/etc/pam.d/passwd <br>
/etc/passwd</code>

6) Eksekver kommandoen sudo find /etc/ -name pass*.

<code>/etc/pam.d/passwd <br>
/etc/passwd <br>
/etc/passwd-</code>

De næste kommandoer bruges til at finde filer baseret på deres byte-størrelse. De to første trin bruges blot til at generere de to filer, som skal findes.

1) I Home directory, eksekver kommandoen truncate -s 6M filelargerthanfivemegabyte.

2) I Home directory, eksekver kommandoen truncate -s 4M filelessthanfivemegabyte.

3) I roden (/), eksekver kommandoen find /home -size +5M.

<code>/home/filelargerthanfivemegabyte</code>

4) I roden (/), eksekver kommandoen find /home -size -5M.

<code>Mange filer under 5 Megabyte...</code>

5) I Home directory, opret to directories, et der hedder test og et andet som hedder test2.

<code>cd home <br>
mikdir test <br>
mikdir test2</code>

6) I test2, skal der oprettes en fil som hedder test.

<code>
cd test2<br>
touch test
</code>

7) I Home directory, eksekver kommandoen find -type f -name test.

<code>./test2/test</code>

8) I Home directory, eksekver kommandoen find -type d -name test.

<code>./test</code>

## Links
[Basic_Linux_Commands](https://www.hostinger.com/tutorials/linux-commands)