---
 hide:
#   - footer
---

## Instruktioner

1) I Home directory, eksekver kommandoen touch minfile.txt

<code>minfile.txt</code>

2) I Home directory, eksekver kommandoen cp minfile.txt kopiafminfile.txt

<code>minfile.txt kopieres til kopiafminfile.txt</code>

3) I Home directory, eksekver kommandoen mkdir minfiledir

<code>minfiledir direktiv oprettes</code>

4) I Home directory, eksekver kommandoen, mv minfile.txt minfiledir

<code>minfile.txt flyttes til minfiledir direktiv</code>

5) I Home directory, eksekver kommandoen, rm -r minfiledir

<code>Rekursivt fjerner minfiledir og dens indhold</code>

I de næste trin skal der arbejdes med oprettelse af filer med tekst indhold, her bliver redirect operatoren introduceret (>). Operatoren tager outputet fra kommandoen på venstre side og skriver til filen på højre side.

1) I Home directory, eksekver kommandoen echo "hej verden" > hejverdenfil.txt

<code>Printer hej verden i konsol og til filen hejverdenfil.txt</code>

2) I Home directory, eksekver kommandoen cat hejverdenfil.txt

<code>Udskriver hejverdenfil.txt's indhold til konsol</code>

3) I Home directory, eksekver kommandoen echo "hej ny verden" > hejverdenfil.txt

<code>Overskriver hejverdenfil.txt med teksten "hej ny verden"</code>

4) I Home directory, eksekver kommandoen cat hejverdenfil.txt

<code>Udskriver hejverdenfil.txt <br> hej ny verden</code>

5) I Home directory, eksekver kommandoen echo "hej endnu en ny verden" >> hejverdenfil.txt

<code>Tilføjer "hej endnu en ny verden" til filen hejverdenfil.txt</code>

6) I Home directory, eksekver kommandoen cat hejverdenfil.txt

<code>Udskriver hejverdenfil.txt <br>hej ny vern hej endnu en ny verden</code>

I de næste trin introduceres pipe operatoren (|). Den tager outputtet fra kommandoen på venstre side, og giver det videre til kommandoen på højre side

1) I /etc/, eksekver kommandoen cat adduser.conf.

<code>Manual til tilføjelse af user</code>

2) I /etc/, eksekver kommandoen cat adduser.conf | grep 1000.

<code>Default: FIRST_UID=1000, LAST_UID=59999 <br>
FIRST_UID=1000 <br>
Default: FIRST_GID=1000, LAST_GID=59999<br>
FIRST_GID=1000</code>

3) I /etc/, eksekver kommandoen cat adduser.conf | grep no.

<code>for system users, system groups, non-system users and non-system groups
Specify whether each created non-system user will be
non-system users are placed into.
The permissions mode for home directories of non-system users.
If set to a nonempty value, new users will have quotas copied
When populating the newly created home directory of a non-system user,
files in SKEL matching this regex are not copied.
list of groups that new non-system users will be added to
if ADD_EXTRA_GROUPS is non-zero or set on the command line.
newly created non-system users to the list of groups defined by</code>

4) I /, eksekver kommandoen grep no /etc/adduser.conf

<code>Samme output som tidliger opgave</code>

5) I /, eksekver kommandoen ls -al | grep proc

<code>dr-xr-xr-x 208 root root          0 Feb 13 02:47 proc</code>

6) I /etc/, eksekver kommandoen ls -al | grep shadow

<code>-rw-r-----   1 root     shadow    1086 Nov 30 14:00 gshadow <br>
-rw-r-----   1 root     shadow    1082 Nov 30 14:00 gshadow- <br>
-rw-r-----   1 root     shadow    1428 Nov 30 11:56 shadow <br>
-rw-r-----   1 root     shadow    1330 Nov 30 11:54 shadow-</code>

## Links
[Basic_Linux_Commands](https://www.hostinger.com/tutorials/linux-commands)