---
 hide:
#   - footer
---

## Instruktioner

1) Eksekver kommandoen pwd.

<code>/home/kali</code>

2) Eksekver kommandoen cd ..

<code>/home</code>

3) Eksekver kommandoen cd /

4) Lokationen / har en bestemt betydning i Linux-filsystemet. Hvad er det? (Her kan du med fordel søge svar på Google).

<code>Systemets rod</code>

5) Eksekver kommandoen cd /etc/ca-certificates/
Hvad findes der i directoriet?

<code>ls</code>

<code>update.d</code>

6) Hvor mange directories viser outputtet fra pwd kommandoen når den eksekveres i directoreit fra forrige trin?

<code>/etc/ca-certificates</code>

<code>2</code>

7) Eksekver kommandoen cd ../..

<code>/</code>

8) Hvor mange directories viser outputtet fra pwd kommandoen nu?

<code>/</code>

<code>1</code>

9) Eksekver kommandoen cd ~ (Karakteren hedder tilde. I Ubuntu kan den nogen gange findes med knappen F6).

10) Kommandoen ~ er en "genvej" i Linux, hvad er det en genvej til?

<code>/home/kali</code>

11) I filsystemets rod (/), eksekver kommandoen ls.

<code>cd /</code>

<code>ls</code>

12) I brugerens home directory, eksekver kommandoen touch helloworld.txt.

<code>cd ./home</code>

<code>touch helloworld.txt</code>

13) I brugerens home directory, eksekver kommandoen nano helloworld.txt.

<code>nano helloworld.txt</code>

14) List alle filer og mapper i brugerens home directory.

<code>helloworld.txt kali</code>

15) List alle filer og mapper i brugerens home directory med flaget -a.

<code>.  ..  helloworld.txt  kali</code>

16) List alle filer og mapper i brugerens home directory med flaget -l.

<code>
total 4<br>
-rw-r--r--  1 root root    0 Feb 13 03:43 helloworld.txt<br>
drwx------ 16 kali kali 4096 Feb 13 03:33 kali
</code>

17) List alle filer og mapper i brugerens home directory med flaget -la.

<code>
total 12 <br>
drwxr-xr-x  3 root root 4096 Feb 13 03:43 .<br>
drwxr-xr-x 18 root root 4096 Sep  2 22:49 ..<br>
-rw-r--r--  1 root root    0 Feb 13 03:43 helloworld.txt<br>
drwx------ 16 kali kali 4096 Feb 13 03:33 kali
</code>                                                

18) I brugerens home directory, eksekver kommandoen mkdir helloWorld.

<code>mkdir helloWorld</code>

19) Eksekver kommandoen ls -d */.

<code>helloWorld/ kali/</code>

20) Eksekver kommandoenn ls -f.

<code>helloworld.txt helloworld .. kali .</code>