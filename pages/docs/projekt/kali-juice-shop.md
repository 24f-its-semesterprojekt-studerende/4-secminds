---
 hide:
#   - footer
---

### Introduktion

Vi anvender en Kali-VM til at hoste Juice Shop hjemmesiden.

### Kali og Juice Shop setup

Kali-VM ved navn "Juice" opsættes.

![kali-vm-juice](../images/projekt/juice-shop-kali/juice-kali-vm-name.png)

Den tilkobles bridge2, da det er vores DMZ netværk.

![kali-vm-bridge](../images/projekt/juice-shop-kali/kali-juice-bridge.png)

NodeJS installeres

![nodejs](../images/projekt/juice-shop-kali/nodejs.png)

NPM installeres

![npm](../images/projekt/juice-shop-kali/npm.png)

Juice Shop hentes, således kali kan hoste hjemmesiden på port 3000

Denne guide [Juice Shop på Kali](https://www.geeksforgeeks.org/how-to-install-owasp-juice-shop-on-kali-linux/) anvendes

![juice-shop-git](../images/projekt/juice-shop-kali/git-clone-juice.png)

### Wazuh-Agent setup

Wazuh Agent sættes op for denne maskine. Det noteres at WAZUH-MANAGER: 10.34.20.102 er IP-adressen for Wazuh-Serveren.

![wazuh-agent-setup](../images/projekt/juice-shop-kali/wazuh-agent-setup.png)

I agentes ossec.conf tilføjes følgende log til listen af overvågede logs:

![wazuh-agent-ossec.conf-logs](../images/projekt/juice-shop-kali/ossec.conf-logs-png.png)

### Fejlfinding

Nu skulle dashboarded vise logs for aktivetet på denne maskine, men det blev der ikke.

Der kigges i logs.

![no_logs](../images/projekt/juice-shop-kali/no_syslogs.png)

Det ses, at der ingen syslogs/auth.logs eksisterer.

Følgende kommando eksekveres, således at logs bliver oprettet

![logs_fix](../images/projekt/juice-shop-kali/syslogs_fix.png)

Wazuh-Dashboard kan nu læse relevante logs for Kali-VM efter et forsøgt brute force angreb.

![wazuh-dashboard-juice-shop-brute-force-detect](../images/projekt/juice-shop-kali/wazuh_juice_logs.png)


### Links
- [Juice Shop på Kali](https://www.geeksforgeeks.org/how-to-install-owasp-juice-shop-on-kali-linux/)