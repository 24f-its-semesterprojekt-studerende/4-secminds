---
 hide:
#   - footer
---

### Introduktion

Vi anvender en Wazuh-server (manager), til at detektere hændelser på Wazuh-agenter (JuiceShop)

### Wazuh-server opsætning

[Følgende opsætningsguide er fulgt](https://24f-its-semesterprojekt-studerende.gitlab.io/4-secminds/systemsec/uge10/25.1_setting_up_wazuh/)


Efter serveren er sat op, så ændres der følgende i dens ossec.conf fil:

Firewall-drop kommando:

Name: navnet på kommandoes - firewall-drop
Executable: script filen, som udføres når denne kommando bliver kaldt
Expect: Input source-ip, som bliver brugt i script filen (den ip som blokeres)
Timeout-allowed: Definer om kommandoen må blokere

![firewall-drop](../images/projekt/wazuh/firewall-drop.png)

Active response:
Command: Kommandoen, som skal kaldes, når dette aktive respons udføres
Location: Hvilke agenter, som skal udføre dette respons. Local = den agent, hvor wazuh serveren har detekteret et incident, som skal reponderes på. All = alle agenter.
Rules_id: Hvilke Wazuh-regler som trigger dette respons. 5763: ssh brute force attack, 31103: SQL Injection, 31106: Web attack returned code 200.
Timeout: Hvor længe den angrebne IP-adresse skal være blokeret. 180 sekunder.

![active-response](../images/projekt/wazuh/firewall-drop.png)

### Links
-