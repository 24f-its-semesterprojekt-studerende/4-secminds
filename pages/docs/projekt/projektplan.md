---
 hide:
#   - footer
---


## Introduktion

## Formål med projektet

## Projektstyring

## Projektbeskrivelse

1. Åbent netværk med Juice Shop kørende
2. MITRE Attack TTP anvendes til at finde admin password på Juice Shop

    - Active Scanning
    - Brute Force
    - Sandworm?

3. Implementer foranstaltninger på netværket
    - Firewall regler
    - Wazuh-detektering
    - ?s

4. MITRE Attack TTP gentages
5. MITRE Attack afværges (forhåbentlig)

## Tidsplan

#### Uge 19: 

- Problemformulering
- Afgrænsning af projekt
- Opsætning af rapport-formalia
- Purple teaming research
- Opsætning af sårbar applikation
- Opsætning af netværk uden blue team foranstaltninger samt back ups!

#### Uge 20:
- Iterativ revurdering af tidsplan
- Afgrænsning
- Red team research
- Red team MITRE Angreb på sårbar webapp
- Blue team research
- Blue team MITRE detections og mitigations på netværk

- Rapportskriving
    - Indledning
    - Problemformulering
    - Gloseliste
    - Afgrænsning
    - Analyse/Metode (påbegyndes)
    - Diskussion (påbegyndes)

#### Uge 21:
- Iterativ revurdering af tidsplan
- Afgrænsning!

#### Uge 22: 
- Iterativ revurdering af tidsplan
- Færdiggør rapport