---
 hide:
#   - footer
---

### Introduktion

Vi anvender en simpel - men effektiv SQL Injection fremgangsmåde for at tilgå hjemmesidens admin bruger.

Formålet med dette er at gidseltage hjemmesiden i form af Ransomware. 

### Fremgangsmåde

#### SQL Injection

Følgende SQL Injection udføres på login-siden

![simple_sql_injection](../images/projekt/juice-shop/simple_sql_injection_admin.png)

Hjemmesiden har ingen foranstaltninger til dette, da vi får adgang til admin brugeren samt dens tilknyttede e-mail adresse, i det denne SQL Injection retunerer den første oprettede bruger i deres database.

![admin_login_success](../images/projekt/juice-shop/admin_login_success.png)

Vi forsøger at ændre passworded for admin for at tage admin-brugeren som gidsel.

![change_admin_pw](../images/projekt/juice-shop/change_admin_pw.png)

Det ses, at vi skal anvende det nuværende password for at foretage en ændring. 

Derfor udnytter vi den nyfundne e-mail adresse til admin brugeren til at udføre et wordlist angreb vha. WFUZZ for at få admins password.

![wfuzz_wordlist](../images/projekt/juice-shop/wfuzz_attack.png)

Angreb returnerer code 200 for password = admin123.

Passworded for admin brugeren kan nu ændres - hjemmesiden er nu taget gidsel.

![changed_pw](../images/projekt/juice-shop/changed_pw.png)