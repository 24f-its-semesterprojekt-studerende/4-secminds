---
 hide:
#   - footer
---

## Hvordan kan man bruge purple teaming til at optimere IT-sikkerheden for et givent netværk?

#### Hvilke værktøjer kan anvendes indenfor blue teaming?
- Wazuh-detektering
- Defense in Depth
- MITRE Attack Mitigations og Detection

#### Hvilke værktøjer kan anvendes indenfor red teaming?
- MITRE Attack Taktikker, Teknikker og Procedurer (TTP)
