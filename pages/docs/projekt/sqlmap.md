---
 hide:
#   - footer
---


## Introduktion

SQLmap er et open source penetreringsværktøj, som bruges til at finde SQL sårbarheder for f.eks. en hjemmeside

## Fremgangsmåde

SQLmap installeres på en vm, således denne vm kan angribe JuiceShop:

![sqlmap_install](../images/projekt/sqlmap/sqlmap_install.png)

Følgende kommando eksekveres:


![sqlmap_command](../images/projekt/sqlmap/sqlmap_command.png)

Resultatet af ovenstående kommando viser funden sårbarhed for "email" input felt.

![sqlmap_result](../images/projekt/sqlmap/sqlmap_par_result.png)

## Links

- [SQLMap dokumentation/usage](https://github.com/sqlmapproject/sqlmap/wiki/Usage)