---
 hide:
  #- footer
---
 
#### Pre-interview
- Etabler regler for interview ang. Data (kvalitativt/kvantitativt), medvirkende, optagelse, tone, længde, opdeling i flere bid, gennemgang af formål, anonymiseret interview, tidsrum for lagring af interview data, forventet tidsrum for svar,

#### Interview
- Introduktion til emne(r), overfladisk overblik og dybdegående samtale inkl. Eventuelle sidespring i tangens med emne(r). Personlig erfaring på både godt og ondt, hvilke forbedringer kunne laves. Er teknologien god nok, kan den forbedres?

#### Slut-interview:
- Tak for interview, forklaring af datas behandling, transskribering, udgivelse på enten lyd, video eller tekst. Holde en god tone og kontakt.

#### Behandling af data
- Emnedata tilsendes den interviewede så der er rette linjer ang. behandling af data, ingen misforståelser kan opstå. I transskribering huskes at tilføje tone af interview, heriblandt fx (latter, vredesudbrud, hårde eller slappe toner etc.) Inkludere visuelle grafer for data.
For at transskribere få en 3. part til at lave denne så der ikke opstår uanede fordomme fra original interviewer.

#### Accept af konklusion
- Skrive en konklusion af data, bede om accept hvis muligt eller finde ud af diverse konflikter i konklusion.
